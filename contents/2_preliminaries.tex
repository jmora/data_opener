\section{Preliminaries}

\contents{sec:preliminaries}{{\auth} establish some notions that should be made clear for the rest of the {\paper}}

%{\Auth} make some assumptions in this {\paper} that do not have an impact on the generality of the approach but ease the explanation.
%In this section {\auth} explain these assumptions and how the presented approach can be generalised to different contexts.
In this section {\auth} introduce some concepts that will be used through the {\paper} and provide some context for the remainder of the {\paper}.

\subsection{Data indexes and repositories}
Data repositories store data, normally in the form of datasets, while data indexes only index the data that may be stored somewhere else.
The confusion between both is logical due to the fact that data repositories often provide some index to search in their contents.
%This index may have an associated repository that stores the datasets or they may be linked from a variety of other repositories.
%\iftoggle{extended}{Without loss of generality}{W.l.o.g.} {\auth} will focus on an index in the current {\paper}

Each approach has different advantages and disadvantages.
On the one hand, data repositories provide an integrated service, which is more convenient for users, and the index can be updated automatically as new data is added to the repository, keeping both index and repository syncronised.
On the other hand, data indexes find less limitations to scale, not only because of the physical space required to store the data but also by removing the need to upload the data.
Indeed, a data index may index data from several repositories, indexing the union of them, and scaling as such.

In the present {\paper} {\auth} focus on the indexing capabilities, either in a data index or present in some data repository.
{\Auth} propose a series of techniques that are independent on the physical storage of the actual data, as long as the data can be accessed to perform the required computations.
In particular, {\auth} focus on providing additional metainformation and search capabilities to find the relevant datasets (or pieces of data) and to ease the use of the information contained in them.


\subsection{Data formats}
Data may be presented in many different formats.
For example, data may be a table which can be stored in a \emph{comma separated values} (CSV) file.
Similarly, \emph{spreadsheet} programs generate files (e.g. Excel and .xlsx) that store several tables, possibly establishing relations among those tables.
\emph{Hierarchical} structures, like XML or JSON are also possible.
\emph{Knowledge representation} formats like RDF or OWL can capture the \emph{semantics} of the relations between the different pieces of data and may be serialised in several different formats, including XML and JSON.

\emph{Databases} and \emph{knowledge bases} may store the information in any of the previous formats and more sophisticated ones.
In the case of relational databases, the information is stored as tables that relate with each other by defining ``foreign keys''.
\emph{Triplestores} keep these relations as explicit predicates, normally focusing on more elaborated schemata, for instance defining \code{SubpropertyOf} and \code{SubclassOf} relations between the elements stored.
\emph{Document oriented databases} store the information in ``documents'' that are hierarchical pieces of data and therefore serialisable to JSON and XML.

Finally, in some cases the data will be stored in some unknown format and queried through an \emph{application programming interface} (API), in such cases the relevant format will be the one used in the aswers to the queries, as well as the format for the queries themselves.

In this {\paper} {\auth} cover all these data formats, since the approach taken is agnostic on the data representation format that is used.
Hierarchical data formats can be flattened to relational data formats by inverting the references, i.e. 
Relational data formats can be reduced to a single table by performing a join between all tables, possibly an outer join if empty values are permitted.
Flattening the data in such a way may not be efficient for data access purposes or the analysis presented, however, it allows generalising the techniques from a theoretical point of view without loss of generality.
This means that the techniques presented in this {\paper} can potentially be refined, taking into account the format in which the data is presented, this optimisation (if possible) is left for future work.

\subsection{Human intervention}
In this {\paper} {\auth} propose a series of techniques that can be applied in an automatic fashion to \iftoggle{extended}{improve the usability of data repositories.}{summarise and characterise datasets.}
Some techniques require previous manual actions, for example the annotation of the data with some schema or providing URIs to describe the contents of the data semantically.
Even if these previous actions are considered manual, they can be automated to different extents, however, this automation is still an open research problem, for each one of these actions.

For some techniques, {\auth} assume that these actions have already been performed.
If the actions have not been performed then the respective techniques will not be applicable and will not be applied but the techniques that do not have these requirements will be applied, according to a \emph{pay-as-you-go approach}.
If the actions have been performed automatically by some system then the quality of the results of applying the techniques covered in this {\paper} will depend on the quality of these previous automatic actions.
Finally, if the actions have been performed manually or the results of some automatic processing have been curated manually then the results obtained with present techniques are guaranteed to be optimal according to the criteria specified.

In any case, the techniques are performed as a best-effort approach to improve the usability of the data available on the web while requiring the least human intervention (and specially commitment) possible, as means to improve the usability, scalability and specially applicability of the presented techniques.

\subsection{Differential frequency}
The frequency of some element or property in a set of elements if the number of times that element appears in that set.
Given a filtering function $\filter$ that returns the multiset of elements matching some condition $\condition$ among those in a original multiset $\set$, and with $|X|$ representing the cardinality of $X$, we can define the frequency for some property in a multiset $\set$ with the formula:
\[\freq(\condition, \set) = \frac{|\filter(\condition, \set)|}{|\set|}\]

A more interesting notion is the differential frequency, of elements matching some condition $\condition$ in a set of multisets.
In this case, the frequencies of the different elements (or properties) are normalised for the global set (the union of all sets) and sets of elements are characterised by the way in which they differ among themselves.
For example the elements to consider may be terms (lemmas) in documents, where each document is a multiset of lemmas and the set of all documents is a corpus considered for some particular application.
The filtering function would therefore consider each one of the lemmas in the documents and would be used obtain the differential frequency of the lemmas in the documents with respect to the corpus.

Given some condition $\condition$ and filter $\filter$ functions defined as before and using the global set $\globalunion = \set_1 \cup \ldots \cup \set_n$  as the union of the individual sets $\set_1, \ldots \set_n$, the notion of differential frequency of elements matching condition $\condition$ in the dataset $\set$ with respect to a global set $\globalunion$ can be formalised according to the next formula:
\[\difffreq(\condition, \set, \globalunion) = \frac{|\filter(\condition, \set)| \times |\globalunion|}{|\set| \times |\filter(\condition, \globalunion)| }\]

This measure is useful to characterise individual sets differentiating them from other sets that may share some characteristics.
In the case of natural language (lemmas, documents and corpus), most frequent words like prepositions and articles are very frequent in all of the documents and therefore they are not relevant for the characterisation of each one of the individual documents.
The differential frequency filters these words and allows to characterise the documents based on what makes them different from the other documents in the corpus, i.e. the characteristics of the document when compared with others and not the characteristics of the language (from a global or general perspective).

\subsection{Mereological features}
We can intuitively define a mereology as a tree (sometimes a graph, depending on cycles) whose nodes are related by \code{partOf} relations.
By mereological features {\auth} refer to those that correspond to elements in a mereology.
For example ``month'' is \code{partOf} some ``year'' and some features (e.g. economic indicators) refer to the month or the year in which they apply.
In such cases, some features (economic indicators) are specified for a range and with some granularity level according to some mereological feature (time for this example).

Mereological features have a special relevance because they define some context for the other features.
This context refers to the circumstances in which the other features are valid, which is specially useful to find the datasets that are relevant for a given purpose.
This context also allows to combine datasets either horizontally (additional features) or vertically (additional observations for the same features) based on how these contexts relate.
When the context overlaps (same values for the mereological features) then the possible combination is horizontal.
On the contrary, when he context does not overlap (at least to some extent) then the combination is vertical, obtaining a new dataset with a context that is the union of previous contexts.
{\Auth} will elaborate more on the combination of datasets in section \cref{sec:greatdataset}.

Temporal and spatial dimensions may be the most common mereological features, we have already seen an example referring to months and years.
Similarly, geographical regions are grouped forming greater regions.
Other features may similarly define groups of elements and elements that are part of those groups.
We can find another usual example in demographics.
When considering the demographics of some region there may be several different ways to split the population into groups, either by age, gender, studies, professional sector, income level, etc.

Similarly to taxonomies, mereologies are more interesting when there is some depth in them.
As the depth increases we say that the granularity increases with it.
In the case of ``professional sector'' we can see that the depth that can be achieved will depend on the different granularity levels that are considered, as many different levels of specificity are possible in this case.

Even though taxonomies and mereologies are often confused, they are semantically different, therefore their meaning and implications are different.
Despite of that, sometimes crossing the line between the two can be convenient and preserve semantic correction.

For instance, we can say that ``Boy'' is subclass of ``Young'' and ``Man'', $\mybox{Boy} \sqsubseteq \mybox{Young} \sqcap \mybox{Man}$.
Having defined these classes we can define the sets of individuals that belong to these classes, respectively: $\mybox{SBoy} = \{x \mid \mybox{Boy}(x)\}, \mybox{SYoung} = \{y \mid \mybox{Young}(y)\}$ and $\mybox{SMan} = \{z \mid \mybox{Man}(z)\}$.
Consequently, the set of boys is a subset of the intersection between young and man: $\mybox{SBoy} \subseteq \mybox{SYoung} \cap \mybox{SMan}$.
Therefore when speaking about the sets of boys, young people and men we can define mereological relations as $\mybox{SBoy} \mybox{partOf} \mybox{SYoung}$ and $\mybox{SBoy} \mybox{partOf} \mybox{SMan}$.
In this case, the change to set semantics should be noted.