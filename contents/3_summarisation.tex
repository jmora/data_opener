\section{Summarisation techniques}
\contents{sec:summarisation}{{\auth} describe techniques for the summarisation of data}

Reducing the size of some data through summarisation helps to process it more efficiently and in ways that would otherwise not be possible.
The summary of the data may be performed on the number of features of the data or the elements displaying these features, we explore both options in this section.
Finally, data visualisation provides a compact representation of the data, especially useful for humans, {\auth} study the feasibility of automatic generation of relevant charts.

\subsection{Sampling and casuistry}
We can select some elements from the dataset to create a summary that preserves the characteristics of the original.
These characteristics will depend on the purpose of the summary.
In most cases, the \emph{sample} should be statistically representative of the data, discarding outliers.
\iftoggle{extended}{However, in some other cases, the purpose may be covering the \emph{casuistry} of the data, and then outliers should be included.}{On the contrary, outliers should be included when the focus lies on the \emph{casuistry} of the data.}
Therefore, the element selection can be refined by detecting outliers and ensuring that
{\pst} \iftoggle{extended}{outliers are excluded in the representative summary and statistical measures preserve similar values,}{statistical measures preserve similar values in the  summary,} and
{\nst} outliers are present in the casuistry and its variance is maximised.

Outlier detection is \iftoggle{extended}{a well studied topic that}{well studied and} approaches can use a variety of criteria\iftoggle{extended}{}{ (e.g. \furl{Chauvenet}{http://www.webcitation.org/6Si5EFyzq})}, usually involving some \iftoggle{extended}{heuristics, either setting a threshold for the distance from the mean or setting a number of partitions for the data.}{heuristics.}
\iftoggle{extended}{An example is Chauvenet's criterion~\cite{chauvenet1863}.
A different option is setting a number of intervals for a partitioning of the data (similarly to a histogram), if some partition is empty then the elements in partitions further from the mean are considered outliers.}{}

\subsection{Granularity reduction}
We can reduce the granularity by grouping several elements (as \code{group by} in SQL) and then aggregating the values for the feature whose granularity is being decreased.
This operation is equivalent to the \emph{``drill up''} operation in an \furl{OLAP cube}{http://en.wikipedia.org/wiki/OLAP\textunderscore cube}.
The dimension on which the elements are grouped will often be spatial or temporal, but it can be any \emph{mereological} dimension, i.e. elements are grouped together because they are part of a greater entity.
For instance, some data may be available by car model and may be grouped by manufacturer or group of manufacturers.

Semantic information about the features is required to reduce their granularity, in particular the aggregation will be:
{\pst} an \emph{average} for numeric values,
{\nst} a \emph{summation} for counts,
{\nst} the \emph{maximum} for maxima,
{\nst} the \emph{minimum} for minima and
{\nst} a \emph{weighted average} for averages, specifying the dimension for the weight.
Logically, the feature \iftoggle{extended}{over which the aggregation is performed}{used for the aggregation} should be annotated as a mereological feature.

\iftoggle{extended}{Reducing the granularity of the data causes information loss, specially \iftoggle{extended}{with respect to}{w.r.t} statistical measurements like the standard deviation.
Hence, the granularity should be decreased after obtaining these statistics.}{}

\subsection{Model building}
Models provide a more compact view of the data.
Different types of models may be helpful for different purposes, for example the correlation among features can be useful for predictive analysis, while clustering may be useful for classification.
\iftoggle{extended}{The implementation and execution required to build the different models can be prioritised depending on the interest by the users.}{}
These models can be built with unsupervised machine learning techniques, therefore not requiring human intervention.

The data may be summarised as a set of clusters for most relevant features.
\iftoggle{extended}{%
    This can be preferably done with algorithms that are not dependent on setting a specific number of clusters beforehand, for instance Affinity Propagation~\cite{frey2007}.
    Alternatively, the number of clusters can be estimated with some technique or combination of them~\cite{kryszczuk2010} to apply conventional clustering techniques as the famous \emph{k-means}~\cite{lloyd1982,forgy1965}.%
    }{%
    }
These clusters can then be described by specifying
{\pst} their average on each feature (\emph{centroid}),
{\nst} their variance for each feature (\emph{shape}) and
{\nst} their number of elements (\emph{size}).

\iftoggle{extended}{Among the summarisation techniques, model building is the most related with other areas, for example data journalism.
Model building is usual in data journalism, building several models to explain different perspectives on the data and connecting them through an story that eases the comprehension of the data by the readers.
This model building can be done with clustering to reduce the dimensionality of the data and to give a more compact view of it as simply a set of clusters.
Clustering may be the most general technique, as an unsupervised model with few restrictions or conditions on the data.
However, other techniques may be useful in different circumstances, when these conditions are met.

For example, histograms group elements in a set according to their proximity in one dimension.
However, more characteristics of the data should be considered to set the criteria for the grouping.
In the case of a histogram, the radius of the different groups (or clusters) is the same for all groups and defines a partition over the dimension that is considered.
If the histogram considers another dimension, this radius should be set to minimise the variance in the other dimension, while keeping the number of clusters reasonable.

Other types of charts can be useful to understand the data.
In the case of journalism, dealing with the latest news and data, the temporal dimension of the data and its evolution through time have a special significance, therefore suggesting the use of line graphs.
{\Auth} will elaborate more on the automatic visualisation of the data in \cref{sec:visualise}.}{}

\subsection{Feature relevance}\label{sec:feature_ordering}
Some features are more relevant for the abovementioned summarisation.
Ordering the features based on their relevance allows prioritising some of them for further processing or to possibly exclude some, i.e. \emph{feature extraction} setting a threshold.
%To do this ordering we must consider the information that features provide independently and when put into context, e.g. if two features are strongly correlated then the values for the second one probably add little value to the summary.
%This order can be done according to a set of considerations, by order of relevance they are:

Features can be ordered according to a set of characteristics, which are the following ones by order of relevance:
{\pst} \emph{Mereological} features, which allow specifying the ranges over which the data is defined, especially if they can provide information about groups of elements (through granularity reduction).
{\nst} Features that are \emph{correlated} with some other features may not provide additional information about the data and may be discarded.
{\nst} Features that participate in some \emph{visualisation} or that are annotated in more detail are more likely to be relevant in the dataset.
{\nst} Features with a greater (normalised) variance present more \emph{entropy} and they may be particularly useful for building models.

\subsection{Data visualisation}\label{sec:visualise}
The visualisation of the data depends on the characteristics of its features.
There are several different \emph{types of dimensions} to consider depending on their characteristics.
\iftoggle{extended}{
\begin{description}
  \item[\fancyenum{Textual}] (potentially unconstrained) text can be visualised as a term cloud or a differential text cloud~\cite{xexeo2009}.
  \item[\fancyenum{Enumerated}] a enumerated feature contains a finite set of values that repeat frequently in the data.
      Boolean or binary dimensions are a special type of enumerated dimensions.
      Other dimensions can be treated as enumerated after obtaining clusters on them.
  \item[\fancyenum{Numeric}] the most general type and the one that is easier to manage.
      \iftoggle{extended}{Most charts are designed to display numerical data.}{}
  \item[\fancyenum{Geographical locations}] can be as specific as GPS coordinates or as broad as country codes.
    The usual visualisation is a map but may be treated (casted) as a enumerated feature if there are only a few different values.
  \item[\fancyenum{Dates and time}] usually in the x-axis when considered as a timestamp. 
      Time as the duration of a process is a numeric value.
  \item[\fancyenum{Mereological}] there may be mereological relations among all previous types of dimensions.
\end{description}
}{}

To automatically generate the visualisations, the type of dimensions in each chart in the system and at least some in the dataset must be labeled.
For example, for a scatterplot two numeric dimensions and a third enumerated dimension for the shape or color of the dots may be considered.
To select the visualisations to generate, both the combination of features in the dataset and the chart types in order must be explored in order.
In the case of the features, the order is as specified before, including first the most relevant ones.
For the charts, the priority is on those with more dimensions, as they may summarise more information.
\iftoggle{extended}{

}{}
When a match between the characteristics of the chart and the features is found, a chart is generated.
Due to the amount of possible combinations, this can be limited with a threshold.
The threshold may be specified on:
{\pst} the amount of information provided by the features,
\iftoggle{extended}{%
    {\nst} the information provided by their combination,
    {\nst} the number of features to consider,
}{%
}%
{\nst} the number of combinations to test (equivalent to a timeout) or
{\nst} the number of charts to generate,
among other possibilities.
The latter yields the most predictable results.

Please note that some charts perform a summarisation of the results (e.g. a histogram) while other charts do not (e.g. a scatterplot), the latter may require some \iftoggle{extended}{previous summarisation}{summarisation} as explained \iftoggle{extended}{through}{in} this section.