\documentclass[11pt]{article}
\oddsidemargin=.3in
\evensidemargin=.15in
\textwidth=6in
\topmargin=-.5in
\textheight=9in
\parindent=0in

\pagestyle{empty}
\usepackage{hyperref}
\hypersetup{urlcolor=black,linkcolor=black,citecolor=black,colorlinks=true}

\let\oldemph\emph
\let\emph\relax
\newcommand{\goodemph}[1]{\oldemph{\textbf{#1}}}
\let\emph\goodemph

\begin{document}
\setlength{\parskip}{6pt plus1pt minus1pt}

\begin{center}
{\Large Jos\'e Mora} \\[.5pc]
{\Large Statement of interest} \\[.7pc]
\href{mailto:jmora@live.com}{jmora@live.com} \\[5pc]
\end{center}

My interest in the ODI lies on creating something valuable, useful and used.
I fully believe in the objectives of the ODI and the means to achieve them, in \emph{what} is done and in \emph{how} it is done, and I would like to be a part of that.
I believe in the value of that work, and I am interested in doing something valuable.
In the next sections I will describe how different aspects of the position relate with these goals.

\section{Data}

\vspace{-8pt}There are many \emph{definitions} of data.
One of the most popular definitions may be the DIKW pyramid.
However, according to that definition, data would be useless unless it is in fact knowledge.
Information is sometimes considered as data with some syntax, being upgraded to knowledge when semantics are added.
For other authors, information is data that is new, informing some agents about something previously unknown and unexpected.% (entropy, self-information).

In a more general and less technical context, data is often understood as information that is objective, which implies unambiguous semantics.
This data is more valuable when it is put into \emph{context}, e.g. linked data, restricting its potential (subjective) interpretations.

% In any case, for any of the previous definitions and probably for any reasonable definition of data, information and knowledge, the value of all of them in modern societies is beyond question.
% Individuals and companies can improve their decision making by using data, becoming more efficient and achieving a greater progress in all areas and aspects of these areas.
% This is applicable to nearly all actions, the value of data can be better observed by considering the negative effects of ignorance and misinformation, the natural antagonists.

For any definition, the value of data is unquestionable in most modern human activities.
Responsible citizens, when acting as voters or consumers, should be informed.
Politicians and producers can do better with more information about the will of the people and the market.
In some cases data is not meant to be consumed but provided, e.g. data journalism, political transparency, certificates as the ones for fair trade, etc.
In general, the examples permeate every aspect of modern societies.

A particular characteristic of data is the potential to \emph{reuse} it and how its \emph{value evolves} (increases) with its use.
Contrary to other products that see their value decreased (or completely removed) with their use, data is not less valuable after being used.
On the contrary, we can consider that data is more valuable after being used, as its accuracy and correctness are tested with the use.
This is one of the reasons why falsifiability is important in science, at least according to Popper.

\section{Software engineering}

\vspace{-8pt}A particularly valuable type of knowledge is the software, specially the software that allows processing the data and obtaining a greater value out of it.
Software engineering specifies ways to write this software more \emph{effectively} and \emph{efficiently}, assuring a greater \emph{quality}, which only counts positively to the value.
The best practices in software engineering facilitate code reuse and maintainability, which are very important due to the reusable nature of information products.

%My interest in software engineering traces back to the year I started to study ``informatics engineering'', with a first course on ``programming methodology''.
%To date I have devoted countless hours to this field of knowledge but I am still interested in improving my skills in this area and learn as much as I can, as 
I feel the knowledge about software engineering very \emph{empowering}.
Additionally, for me, implementing systems that work is particularly \emph{rewarding}, as theory is meant to be put into practice to be really useful, and having (producing) something that works is something (as opposed to nothing).
I consider that research (next section) is not truly complete until it has been empirically tested, and thus development (at least the initial version) is also part of the research process.

\section{Research}

\vspace{-8pt}There is one thing better than building systems that work and solve problems: building \emph{new systems} that work and solve \emph{new problems}.
Research, as opposed to other applications of software engineering (e.g. consulting) addresses the kind of problems that are open in the state of the art or propose new and innovative solutions to problems previously solved.
I consider this kind of activity particularly exciting and enjoyable.
In short, I like challenges.

Scientific \emph{evaluation} is fundamental to be able to assess to what extent a system works, solves a problem and how well (e.g. efficiently) the problem is solved.
In case these aspects cannot be assessed, then they may as well not be true at all.
\emph{Publishing} and communicating the results is crucial to ensure that the solutions that are proposed are new and hopefully better, as replication of efforts provides little value.

All these aspects of research (novelty, rigorous evaluation and result publication) contribute to a greater value of the work that is done by providing solutions with clearly stated limitations and quality. % that solve a problem once ``and for all'', until the state of the art progresses.
Research acts as a rudder guiding development or, as commonly stated, acting as the ``tip of the spear'', \emph{opening the way} for further development and pushing the boundaries of the state of the art, where most value is to be gained, enabling further work.
``There be dragons'', I cannot think of anything more interesting.

\section{Openness}

\vspace{-8pt}Openness is probably more important than previous points as it is a transversal characteristic that increases the impact of them all.
While previous sections described \emph{what} I find interesting (due to the value and impact implied), this section describes \emph{how} to approach that, in order to increase its value and impact.
In this section I go through the points of the previous sections describing how openness influences them.

\subsection{Open data}

\vspace{-8pt}As previously stated, data increases its value as it is used (as opposed to other products).
Not only data must be open, but it has to be correctly opened to develop its full potential.
If some data is open but impossible to find, understand or use (in general) then it is not going to be used.
Some of my interests in this regard are in the \emph{report} previously sent and more of them are in the extended version\footnote{\url{http://dx.doi.org/10.6084/m9.figshare.1176765}}.


\subsection{Open source}

\vspace{-8pt}Similarly to data, open source code increases its value as it can be reused and it is not worn down by the use.
Similarly to research, openness for code provides visibility and impact.
It does also allow reusing solutions, especially when it is not pseudo-code in a document but working code in a repository.
Additionally to previous points, open source encourages greater \emph{quality} in the software, which is particularly relevant when some legacy code should be reused.
%The amount of pain that can be avoided with this cannot be overstated.
Code is better open.

\subsection{Open research}

\vspace{-8pt}There are currently three strong \emph{trends}:
 \vspace{-4pt}\begin{itemize}
	\item \vspace{-8pt} Open access for publications is being promoted and even enforced in some cases.
	\item \vspace{-8pt} Datasets are in their way to become first class citizens among publications.
	\item \vspace{-8pt} Data science and data intensive research are hot topics.
\end{itemize}
\vspace{-10pt}These are three ways in which data and openness are changing research, and with it the progress in all research areas.
In my humble opinion, these trends are an indication of the current relevance of open data and how it may (greatly) impact the future.

\section{Additional considerations}

\vspace{-8pt}Finally, I would like to add some considerations about other aspects that are relevant.

London is a great city where probably anyone would like to live, and it surely offers many opportunities for collaboration with other organizations in the same city.
The \emph{location} of the ODI, so central in London, multiplies the impact and the value of initiatives like lunch time lectures.
This opens, facilitates and boosts many and very interesting possibilities.

English has become the world \emph{language} and the lingua franca for computer science, academia in most areas, and certainly the intersection between both.
I would certainly like to live in a country where English is spoken as the main language, more than any other, as I think my English level is good enough for this, and improving it is always possible and interesting.

Last but not least, the leading countries with respect to transparency and data openness seem to be all English speaking.
In this regard, maybe paradoxically, my personal and humble opinion is that the progress with respect to transparency in leading countries contributes to transparency in other countries more than the direct attempts and initiatives that occur in them.
The reason is the strength of evidence, examples and comparisons.
Projects in countries like Spain have a limited impact as their success is jeopardised, restricted and hindered by many different actors and factors.
Having a strong \emph{example} to follow and success stories for motivation would greatly simplify the whole process, more than many current initiatives that may be considered in these countries.


%PS: I find this statement of interest plagued with references to value, yet the interest of doing something valuable may not be clear.
%Value is defined in different ways for means and ends.
%Ends have some value on their own, while means obtain their value as they contribute to some end.
%For me, life is not and end but a mean, as it is temporary.
%What is achieved through life, what is done, remains forever, as the past and its consequences, while the life can be lost at any time with no guarantees.
%What is done is what gives life its meaning, and what is done has to be done with a purpose.
%And I don't know what I'm saying anymore...
%But in short, no value = useless = meaningless = waste of time. I'm not interested in that, but in the opposite, I don't have as many hours as I would like in my life, better do something useful and valuable with them.

\end{document}