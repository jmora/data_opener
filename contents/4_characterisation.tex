\section{Characterisation techniques}
\contents{sec:characterisation}{{\auth} describe techniques to characterise the datasets}
The characterisation of the datasets provides an intensional description of their contents.
For indexes or repositories, this description can be used for the classification of the datasets, what allows to index them as a hierarchy and perform faceted search on them.
For \iftoggle{extended}{users}{users (both people and software agents)}, the description can be used to \iftoggle{extended}{decide more easily}{determine} the relevance of a dataset for their interests.
For example, we have already seen the LODSS, which concisely informs about some characteristics of the data.
In this section we explore additional \iftoggle{extended}{characteristics of the data}{data characteristics} that may be relevant to find the datasets that suit the needs of some user.

\subsection{Statistics}
Some statistics about the data may provide a fair idea about the dataset contents.
This is specially relevant for numeric data, similarly to the \code{summary} function in \furl{R}{http://www.r-project.org/}.
Examples \iftoggle{extended}{of these statistical indicators are}{are}: maximum, minimum, mean, standard deviation, and the values for the quartiles.
\iftoggle{extended}{Temporal and spatial dimensions can be summarised as a set of ranges and a granularity level.}{}
Several statistics from textual dimensions can be extracted from the words, their stems and preferably their lemmas.
For example, the (differential) frequency of terms can be taken into account and most frequent terms can be listed with their frequency.

\subsection{Data quality}
Quality is defined as the \emph{fitness for a given use} and it has been extensively studied \iftoggle{extended}{for data~\cite{mendes2011}}{for data (\furl{link to survey}{http://static.lod2.eu/Deliverables/Deliverable\textunderscore 4.3.1\textunderscore FP7\textunderscore LOD2\textunderscore 20110131.pdf})}.
\iftoggle{extended}{Additionally to the data in the dataset and annotations on the features, there may be some metainformation to specify about the dataset itself.}{In this regard, some metadata can be specified about the dataset.}
Examples are: files and their formats, authorship, provenance (possibly linking datasets),\iftoggle{extended}{ date when the dataset was last updated,}{} versioning and state of the dataset, licenses and terms of use, and size.

In those cases where datasets are not static \iftoggle{extended}{pieces of data}{data} but \iftoggle{extended}{a service accessed}{accessed} through an \iftoggle{extended}{application programming interface (API),}{API,}
specifying some characteristics about this API may prove useful.
Examples are: access methods, costs, quotas, update frequency and timeliness of the data (for how long the data is valid).


\subsection{Ranges and granularity}
The part of the global (or potential) range covered by some dataset is particularly useful for \emph{mereological} features (e.g. spatial and temporal dimensions).
For example, other values will probably depend on the mereological features or a user may be only interested in a range of values for some mereological feature (which may not be present in some datasets).
More importantly, the combination of a dataset with other datasets will depend on the parts of the mereological features that are covered by each of those datasets.

In addition to value ranges, granularity should be considered for \iftoggle{extended}{mereological features, both for user interest and dataset combination.}{these features.}
For example, many economic indicators may be specified for a range of several years with a yearly or monthly granularity.


\subsection{Data schema}
Data may be specified according to some schema or mapped to it.
Some possibilities for these schemata are attribute-value pairs, relational schemata (i.e. database schemata), and ontological models (light and heavy).
The schema information is useful from both semantic and syntactic perspectives and it is useful both for manual and automatic usage of the data, as it provides information about the data (e.g. \emph{datatypes}) and its structure (the \emph{relations} between its elements).
The relevance of the schema can be appreciated for example in \furl{XML Schema}{http://www.w3.org/XML/Schema.html} and \furl{RDF Schema}{http://www.w3.org/TR/rdf-schema/}.

\iftoggle{extended}{It is worth noting that there are techniques (out of the scope of this {\paper}) to improve this description of the data, e.g. matching attributes to predicates in an ontology or automatically extracting the XML Schema from a XML document.}{}



\subsection{Dataset classification}
If features in the data are identified by \iftoggle{extended}{\emph{URIs} of an ontology network or a set of mapped ontologies}{\emph{URIs} from an ontology}, then we can classify them according to that knowledge.
Alternatively, if features are described with a set of terms, then a \emph{folksonomy} can be \iftoggle{extended}{obtained~\cite{lin2009,eda2009,kiu2011},}{obtained,} based on their co-occurrence.

Datasets can be classified according to \iftoggle{extended}{the features considered in them,}{their features,} the classification of these features and the characteristics of the data \iftoggle{extended}{mentioned in}{in} this section.
This classification allows searching for a dataset \iftoggle{extended}{that contains information}{}
{\pst} about a specific feature,
{\nst} related with some particular topic,
{\nst} in a given range of values for some feature,
{\nst} with some granularity level, or
{\nst} updated in the last few days,
among other possibilities, including any combination of the previous criteria.
Searching for some data in a hierarchy may be the best option for humans, while querying for data with some given characteristics may be better suited for software systems.

%We can classify the data based on the classification of its features.
%With this classification we can group different data into classes, e.g. economic indicators, market studies or scientific trials.
%The classification of data adds hierarchy levels to the clusters of data, this allows searching for data in a hierarchy (adding specificity on each level) and faceted search in an index.