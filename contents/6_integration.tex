\section{Integration techniques}
\contents{sec:integration}{{\auth} analyse techniques that can be used to integrate several datasets into a greater dataset, along with its benefits}
There are a number of details that need to be known about datasets to be able to use the information stored in them.
Among these details we can consider the location of the data (e.g. the URL), the format to parse and process it and the methods available to update it (e.g. RESTful).
If users of the data could abstract from these details, using the data would require less effort and considerations, which would make it more usable.

Additionally, we can usually find data that is related but fragmented in several datasets and that they may be related or even complementary (\cref{sec:complementary}).
If the data in these datasets could be \emph{accessed} as a single dataset then the usability of the data would also be increased.

Finally,  we can find an example of this usability through using the data, for example building some visualisation of the data over API that demostrates the usability of both, the integration of several datasets and the abstraction provided by an API.


\subsection{As a greater dataset}\label{sec:greatdataset}

From a general point of view, there are two groups of approaches to perform this: materialisation and virtualisation.
The materialisation approach duplicates the information from the sources that are integrated, which implies potential consistency problems and requires more hardware resources to store the data.
The virtualisation is less efficient when accessing the information, due to network latency, and may require some partial materialisation in some cases.
For instance, if the data is available as a set of CSV files, the virtualisation approach may require to download the files and process them before performing any operation (e.g. a query) on the data available in those files.

There are several options to integrate the information of several datasets.
The most immediate solution is simply to combine the available data withouth performing any type of merge operation on it.
As more semantics about the data are known, more sophisticated solutions become available.
Features (columns) in several datasets that are known to be equivalent can be merged into one (union) when performing the combination.
Individuals (rows) that are known to be the same can similarly be merged into one (join) when performing this union.

\subsection{As a service}
Both groups of approaches (materialisation and virtualistation) can provide access through an API for the information stored in both types of systems (data repositories and data indexes).
However, data repositories are better suited for the materialisation approach, providing access through the API to the information stored in them.
On the contrary, indexes are better suited for a virtualisation approach, possibly offering some sort of mashup capabilities over the indexed datasets.
For the latter, having some form of API over the datasets greatly eases the process, as offering an API may simply require some translation between the used API and the one that is offered.
If this is not possible and the dataset is stored as a set of files, then some sort of materialisation may be required to access the data through an API.

Updating the information may be possible in some cases, in addition to querying or accessing the information.
If the data is stored as a set of files, e.g. CSV files, this may require to download them, modify them and upload them again, assuming the service that stores them allows this.
In some other cases an API may be available to query and update the information.
Again, offering a service and building applications is easier when the datasets are accessible through web services with query (and potentially update) capabilities, which demonstrates the greater usability of the information when presented in such ways.

On a side note, both methods of presentation are compatible, neither an API to access some data nor a set of files to be downloaded prevent the possibility of the other presentation being possible.
Presenting the data with both methods ensures a greater usability than presenting it with only one of them.


\subsection{Integrated visualisation}
Once there is a API that allows querying the whole catalog of datasets as an integrated dataset it is possible to build a visualisation over this integrated dataset.
For a comparison with the state of the art we can consider the \furl{Google Public Data Explorer}{http://www.google.com/publicdata}(\GDE{}) from Google.
There are two limitations that would be overcame when compared with the Google service.

First, the \GDE{} offers a graphical and simple interface to explore the data, but the data itself is not offered, i.e. it can be browsed but it is not open in a processable format.
When some data is displayed in \GDE{} it is possible to link to that view of the data.
Similarly, it should be possible to link to the data itself, offered according to the formats of the API, for instance: RDF, JSON and CSV could be suitable formats.
Keeping the provenance of the data would additionally allow linking the original datasets.

Second, the visualisation process is mostly manual for the \GDE{}.
Users choose the data that they want to visualise and the way in which they want to visualise it.
{\Auth} have proposed several techniques to automatically generate these visualisations in \cref{sec:visualise}, according to the characteristics of the data and the visualisations themselves.
This visualisation can be interactive, with users adding and removing features from the set that is visualised, selecting ranges for these features and choosing among the options to visualise the set of selected features.



\subsection{Scalability considerations}
The approaches discussed in this section are particularly costly and may require vast amounts of resources, both computational and human, therefore some scalability considerations should be made fpr the techniques in this section in particular.

Due to the costs of these techniques, they may only be applicable to a reduced number of datasets.
In a pay-as-you-go approach, providing an API and integrating the datasets that can already be accessed through an API requires less resources, and should be prioritised with a virtualisation approach, which requires less resources.
In these cases, the computations can be performed in the client side (e.g. using Javascript), distributing the computational load, which is a more scalable solution.
Datasets that do not provide an API can individually be materialised and offered through an API for use and integration in those cases where there is a special interest by the community to use them.

Finally, the requirement of human resources can be mitigated by providing some interfaces for data curation by volunteers, i.e. annotating, cleaning, updating and the other tasks we have seen along the {\paper}.
For the computational resources, many results of the computations can be stored so as to not to repeat them.
Scalability issues are further discussed in \cref{sec:scalability}.

