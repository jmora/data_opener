## A repo with the paper LaTeX source files

The pdf can be found on [figshare](http://figshare.com/articles/Increasing_the_usability_of_data_indexes_and_repositories/1176765).

## Abstract

Data repositories and data indexes help to find the relevant datasets among those open and available, as well as hosting them in the case of repositories. However, as the amount of data online increases, finding the relevant data for some given task becomes gradually more challenging. In this paper I propose some techniques to increase the usability of data indexes and repositories, to ease both finding the relevant data and using it. These techniques are meant to increase the usability of data indexes and repositories both for humans and software systems. Scalable techniques for data summarisation and characterisation to obtain a representative oversight of the shape and nature of large and small datasets.